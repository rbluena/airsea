$(document).ready(function () {

  var validationDocId = "";

  // Initializing all the popovers
  $('[data-toggle="popover"]').popover({
    trigger: 'focus'
  });

  // $('#bondValidatioBtn').on('click', function(e) {
  //   var transitId = $(this).data('transitid');
  //   validationDocId = transitId;

  //   console.log(validationDocId);
  // });

  //Validate bonds from modal
  $('#validate-bond-form').on('submit', function(e) {
    e.preventDefault();
    var $this = $(this);
    var validatedDate = $this.find('input').val();
    var url = '/clearance/validate_bond';

    var data = {
      transitId: validationDocId,
      validatedDate: validatedDate
    }

    $.ajax({
      type: "PUT",
      url: url,
      data: data,
      dataType: "json",
      beforeSend: function() {
        $(document.body).css({cursor: 'wait'});
      },
      success: function (response) {
        if(response === 'success') {
          $(document.body).css({cursor: 'default'});
          $this.notify(response, { className: 'success', autoHideDelay: 1500 });

          $this.parents('.form-container').find('.text-success').show(false);
        }
      },
      error: function(err) {
        console.log(err);
      }
    });
  });

  $('#bondValidateModal').on('show.bs.modal', function(e) {
    var button = $(e.relatedTarget);
    validationDocId = button.data('transitid');
    $(this).find('.text-success').hide(false);
  });

  // Changing transit status (Documentation, )
  $('.update-transit-status').on('change', function(e) {
    var $this = $(this);
    var statusValue = $this.val();
    var url = $this.data('url');

    $.ajax({
      method: "PUT",
      url: url,
      data: {statusValue: statusValue},
      dataType: "json",
      beforeSend: function() {
        $(document.body).css({cursor: 'wait'});
      },
      success: function (response) {
        $(document.body).css({cursor: 'default'});
        $this.notify(response, { className: 'success', autoHideDelay: 1500 });
      },
      error: function(err) {
        $this.notify(err, { className: 'danger', autoHideDelay: 1500 });
;      }
    });
  });

  // CHART JS
  // 1) 
  /**
   * Total bond usage
   */
  $.ajax({
    type: "GET",
    url: "/stats/bond_usage",
    dataType: "json",
    success: function (response) {
      var bondData = (response.bondBalance >= 0) ?  [response.usedBonds, response.bondBalance] : [response.usedBonds, 0]
      renderChart(bondData, 'pie');
    },
    error: function(err) {
      console.log(err);
    }
  });

  /**
   * Bond usage by country
   */
  // $.ajax({
  //   type: "GET",
  //   url: "/stats/bond_by_country",
  //   dataType: "json",
  //   success: function (response) {
  //     renderChart(response, 'horizontalBar');
  //   },
  //   error: function(err) {
      
  //   }
  // });



  // SHOWING TABLE ON CLEARANCE INDEX PAGE
  $('#transit-table-data').DataTable({
    "scrollY": "450px",
    // "scrollX": "600px",
    "scrollCollapse": true,
  });


  // updating clients
  $('.clients .table .edit-client').on('click', function(event) {
    $this = $(this);
    $row = $this.parents('tr')
    $hiddingRows = $row.parents('tbody').find('tr');
    $hiddingRows.hide(false);
    $row.show(false);
  });

  // Showing
  var transitsData = $("#transit-table").data('tabular');
 
  // TRANSIT TABLE IN /clearance/export
  $("#transit-table").tabulator({
    pagination:"local",
    height:350, 
    layout:"fitColumns", //fit columns to width of table (optional)
    columns:[ //Define Table Columns
        {title:"Regime", field:"regime", with: 150},
        {title:"Tansad No", field:"tansadNo", width: 150},
        {title:"T1", field:"teeOne", sorter: "string", editor: true, width: 150},
        {title:"Sub-T1", field:"subTeeOne", width: 150},
        {title:"BT Declaration No", field:"BTDeclaration", width: 170},
        {title:"Bond Value", field:"bondValue", formatter: "money", align:"right", width: 120},
        {title:"Bond Validation", field:"bondValidit", align:"center", width: 120},
        {title: "CFA Name", field: 'CFAName', width: 100},
        {title: "Load Package", field: 'loadPkg', width: 100},
        {title: "Package Unit", field: 'packageUnit', width: 100},
        {title: "Load Weight", field: 'loadWeight', width: 100},
        {title: "Weight Unit", field: 'weightUnit', width: 100},
        {title: "Port of Departure", field: 'portOfDeparture', width: 100},
        {title:"Country", field:"destinationCountry.name", width: 100},
        {title:"Vehicle No(Chassis No)", field:"vehicleNo"},
        {title:"Container Number", field:"containerNo", width: 100},
        {title:"Trailer No", field:"trailerNo", width: 100},
        {title:"Semi Trailer No", field:"semiTrailerNo", sorter: "string",  width: 100},
        {title:"Issue Date", field:"issueDate", sorter:"string", width: 100},
        {title:"Departure Date", field:"departureDate", sorter:"string", width: 100},
        {title:"Status", field:"status", width: 100},
        {title:"Border Point/Via-Warehouse1 Date", field: "bondValidationDate", width: 100},
    ],
  });;
  $("#transit-table").tabulator("setData", transitsData);
});

/**
 * Downloading table in excel format
 */
function downloadTable() {
  var fileName = new Date() + '.csv';
  $("#transit-table").tabulator("download", "csv", fileName);
}

/**
 * Rendering Chart from Chart.js
 * 
 * @param {Array} data 
 * @param {String} chartType
 * 
 * @return {void}
 */
function renderChart(data, chartType) {
  if(chartType === 'pie') {
    var ctxPie = document.getElementById("pieBondUsage");

    var pieBondData = {
      labels: ['Debit', 'Balance'],
      datasets: [{
          data: data,
          backgroundColor: ['#007bff', '#343a40'],
      }],
    }
  
    var pieBondUsage = new Chart(ctxPie, {
      type: 'doughnut',
      data: pieBondData,
      options: {
        title: {
          display: true,
          position: top,
          text: "Bond Usage"
        },
        legend: {
          display: true,
          fullWidth: false,
          position: 'bottom',
          labels: {
            // fontColor: 'rgb(255, 99, 132)',
            fontSize: 17
          }
        }
      }
    });
  }

  /* if(chartType === 'horizontalBar') {
    var ctxBarHorizontal = $("#barBondByCountry");
    // var pieBondDataValues = $("#barBondByCountry").attr('pieBondValues');
  
    var horizontalBarBond = {
      labels: ['Tanzania', 'Rwanda', 'Burundi', 'Kenya', 'Uganda', 'Others'], // response[0]
      datasets: [{
          data: [45, 67, 24, 98, 15, 45], // response[1]
          backgroundColor: ['#ffc949', '#ff7d3c', '#f62d47', '#9a2159', '#721264'],
      }],
    }
  
    var barBondByCountry = new Chart(ctxBarHorizontal, {
      type: 'horizontalBar',
      data: horizontalBarBond,
      options: {
        title: {
          display: true,
          position: top,
          text: "Bond Usage"
        },
        legend: {
          display: false,
          fullWidth: false,
          position: 'bottom',
          labels: {
            // fontColor: 'rgb(255, 99, 132)',
            fontSize: 17
          }
        }
      }
    });
  } */
}