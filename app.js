const path = require('path');
const express = require('express');
const logger = require('morgan');
const dotenv = require('dotenv');
const multer = require('multer');
const session = require('express-session');
const flash = require('express-flash');
const expressValidator = require('express-validator');
const bodyParser = require('body-parser');
const passport = require('passport');
const lusca = require('lusca');
const helmet = require('helmet');
const moment = require('moment');
const accounting = require('accounting')
const MongoStore = require('connect-mongo')(session);

const connectDatabase = require('./app/config/DB');

/**
 * Load environment variables
 */
dotenv.load();

const app = express();

connectDatabase();

app.set('view engine', 'pug');
app.set('views', path.join(__dirname, 'app/views'));
app.use('/public', express.static(path.join(__dirname, 'public')));
app.use('/uploads', express.static(path.join(__dirname, 'uploads')));

/**
 * Setting logger
 */
app.use(logger('dev'));

/**
 * Setting body-parser
 */
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
  extended: true
}));

/**
 * Setting session
 */
app.use(session({
  resave: true,
  saveUninitialized: true,
  secret: process.env.SESSION_SECRET,
  store: new MongoStore({
    url: process.env.MONGODB_URI,
    autoReconnect: true,
    clear_interval: 3600
  })
}));

/**
 * Setting authentication
 */
app.use(passport.initialize());
app.use(passport.session());


app.use(expressValidator());
app.use(flash());

/**
 * Setting security against csrf attack
 */
// app.use((req, res, next) => {
//   lusca.csrf()(req, res, next);
// });
// app.use(lusca.xframe('SAMEORIGIN'));
// app.use(lusca.xssProtection(true));
// app.use(helmet());

/**
 * Setting local variables
 */
app.use((req, res, next) => {
  res.locals.SITE_NAME = process.env.SITE_NAME;
  res.locals.SITE_DESC = process.env.SITE_DESC;
  res.locals.user = req.user;
  res.locals.moment = moment;
  res.locals.accounting = accounting;
  next();
});

const User = require('./app/models/User');
const Transit = require('./app/models/Transit');

/**
 * Services
 */
global.userService = require('./app/services/userService')();
global.transitService = require('./app/services/transitService')();
global.fileService = require('./app/services/fileService')();

/**
 * Setting redirect after login
 */
app.use((req, res, next) => {
  if (!req.user &&
    req.path !== '/login' &&
    req.path !== '/register' &&
    !req.path.match(/^\/auth/) &&
    !req.path.match(/\./)) {
    req.session.returnTo = req.path;
  } else if (req.user && req.path === '/dashboard') {
    req.session.returnTo = req.path;
  }
  next();
});

/**
 * Routes
 */
app.use('/', require('./app/routes/home'));
app.use('/auth', require('./app/routes/auth'));
app.use('/users', require('./app/routes/user'));
app.use('/notifications', require('./app/routes/notifications'));
app.use('/cargo', require('./app/routes/cargo'));
app.use('/bond', require('./app/routes/bond'));
app.use('/clearance', require('./app/routes/clearance'));
app.use('/files', require('./app/routes/files'));
app.use('/stats', require('./app/routes/analytics'));


/**
 * Error handling
 */

module.exports = app;
