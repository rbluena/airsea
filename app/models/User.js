const {Schema} = require('mongoose');
const mongoose = require('mongoose')
const bcrypt = require('bcryptjs');

const userSchema = new Schema({
  firstname: {
    type: String,
    trim: true,
    required: true
  },
  lastname: {
    type: String,
    trim: true,
    required: true
  },
  username: {
    type: String,
    unique: true,
    trim: true,
    lowercase: true,
    required: true
  },
  email: {
    type: String,
    lowercase: true,
    trim: true,
  },
  phone: {
    type: String,
    trim: true,
    required: true,
    unique: true
  },
  verificationCode: String,
  firstLogin: { // Checking if user is loggedin for the first time
    type: Boolean,
    default: true
  },
  password: String,
  organization: {
    internal: {
      type: Boolean,
      default: true,
    },
    name: {
      type: String,
      default: 'AirSea'
    }
  },
  level: {
    type: Array,
    default: ['staff'] // admin, staff
  }
}, {
    timestamps: true
  });

// userSchema.pre('save', function(next) {

// });


const User = mongoose.model('User', userSchema);

userSchema.pre('save', async function(next) {
  const user = await find({ username: this.username});

  if(!user) {
   return next();
  }

  const username = this.username + 1;
  return next();
});

User.comparePassword = (password, hash) => {
  return bcrypt.compare(password, hash).then(isMatch => isMatch);
};

User.getAllUsers = () => User.find();


module.exports = User;
