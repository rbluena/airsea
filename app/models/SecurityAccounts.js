const {Schema} = require('mongoose');
const mongoose = require('mongoose');

const securitySchema = new Schema({
  no: Number,
  securityNo: {
    type: String,
  },
  validFrom: {
    type: Date
  },
  validTo: {
    type: Date
  },
  amount: Number,
  activeStatus: {
    type: Boolean,
    default: true // Active, Suspended
  }
});

const Security = mongoose.model('Security', securitySchema);

module.exports = Security;