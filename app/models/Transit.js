const {Schema} = require('mongoose');
const mongoose = require('mongoose');

const transitSchema = new Schema({
  owner: {
    type: String
  },
  clientEmail: {
    type: String,
    lowercase: true,
    trim: true,
  },
  regime: {
    type: String, // IM8, EX3
    trim: true
  },
  subTeeOne: {
    type: String,
    unique: true
  },
  teeOne: {
    type: String,
  },
  CFANumber: String,
  CFAName: String,
  bondValue: {
    type: Number
  },
  goodsDesc: {
    type: String,
  },
  loadPkg: {
    type: String,
  },
  packageUnit: {
    type: String
  },
  loadWeight: {
    type: String
  },
  weightUnit: {
    type: String
  },
  BTDeclaration: {
    type: String,
  },
  tansadNo: String,
  CRN: {
    type: String,
  },
  issueDate: {
    type: Date
  },
  departureDate: {
    type: Date
  },
  containerNo: {
    type: String
  },
  vehicleNo: {
    type: String,
  },
  trailerNo: {
    type: String,
  },
  semiTrailerNo: {
    type: String
  },
  transporter: {
    type: String
  },
  driverName: {
    type: String,
  },
  driverLicense: {
    type: String,
  },
  driverPassport: {
    type: String,
  },
  destinationCountry: {
    code: String, // BI, RW, KE, UG, ZM
    name: String // Burundi, Rwanda, Kenya, Uganda, Zambia, others
  },
  bondInUse: {
    type: Boolean,
    default: true,
  },
  exitStatus: {
    type: Boolean,
    default: false // true or false
  },
  delete: {
    type: Boolean,
    default: false
  },
  checkPoint1: String,
  checkPoint1Date: Date,
  checkPoint1Time: String,
  checkPoint2: String,
  checkPoint2Date: Date,
  checkPoint2Time: String,
  borderPoint: String,
  borderPointDate: Date,
  borderPointTime: String,
  bondValidation: {
    status: {
      type: Boolean,
      default: false
    },
    validatedAt: {
      type: Date,
      default: null,
    }
  },
  status: {
    type: String,
    default: 'No-Activity' // Not-Exited One, Two Three
  }
},{
  timestamps: true
});

const Transit = mongoose.model('Transit', transitSchema);

module.exports = Transit;