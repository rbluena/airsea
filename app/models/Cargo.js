const {Schema} = require('mongoose');
const mongoose = require('mongoose');

const cargoSchema = new Schema({  
  subTeeOne: {
    type: String
  },
  goods: {
    type: String
  },
  issueDate: {
    type: Date
  },
  departureDate: {
    type: Date
  },
  transporter: {
    type: String
  },
  destinationCountry: {
    code: String, // BI, RW, KE, UG, ZM
    name: String // Burindi, Rwanda, Kenya, Uganda, Zambia, others
  },
  status: {
    type: String,
    default: 'Not-exited'
  }
},{
  timestamps: true
});

const Cargo = mongoose.model('Cargo', cargoSchema);

module.exports = Cargo;