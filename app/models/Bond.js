const {Schema} = require('mongoose');
const mongoose = require('mongoose');

const bondSchema = new Schema({
  no: Number,
  securityNo: {
    type: String,
  },
  type: {
    type: String,
    default: 'CB8'
  },
  validFrom: {
    type: Date
  },
  validTo: {
    type: Date
  },
  amount: Number,
  active: {
    type: Boolean,
    default: true // Active, Suspended
  }
});

const Bond = mongoose.model('Bond', bondSchema);

module.exports = Bond;