const express = require('express');
const notifyCtrl = require('./../controllers/notifications');
const passport = require('../config/passport');
const router = express.Router();

router.get('/', passport.isAuthenticated, notifyCtrl.getIndex);

module.exports = router;