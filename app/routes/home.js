const express = require('express');
const homeCtrl = require('./../controllers/home');
const passport = require('../config/passport');
const router = express.Router();

router.get('/', homeCtrl.getIndex);
router.get('/dashboard', passport.isAuthenticated, homeCtrl.getDashboard);

module.exports = router;