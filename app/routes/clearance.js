const express = require('express');
const passport = require('../config/passport');
const clearanceCtrl = require('./../controllers/clearance');
const router = express.Router();

router.get('/', passport.isAuthenticated, clearanceCtrl.getIndex); //  Should be a staff
router.get('/import', passport.isAuthenticated, passport.isAdmin, clearanceCtrl.getImport);
router.get('/export', passport.isAuthenticated, passport.isAdmin, clearanceCtrl.getExport);
router.put('/validate_bond', passport.isAuthenticated, clearanceCtrl.putValidateBond);
router.get('/transits', passport.isAuthenticated, passport.isAdmin, clearanceCtrl.getTransits);
router.get('/:id', passport.isAuthenticated, passport.isAdmin, clearanceCtrl.getShow);
router.get('/:id/edit', passport.isAuthenticated, passport.isAdmin, clearanceCtrl.getEdit);
router.post('/:id/edit', passport.isAuthenticated, passport.isAdmin, clearanceCtrl.postUpdate);
router.get('/:id/delete', passport.isAuthenticated, passport.isAdmin, clearanceCtrl.getDelete);
router.put('/:id/update_status', passport.isAuthenticated, clearanceCtrl.putStatus); // Passport is a staff

module.exports = router;