const express = require('express');
const authCtrl = require('./../controllers/auth');
const router = express.Router();

router.post('/register', authCtrl.validateRegister, authCtrl.postRegister);
router.get('/register', authCtrl.getRegister);
router.post('/login', authCtrl.validateLogin, authCtrl.postLogin);
router.post('/changepassword', authCtrl.validatePostChangePassword, authCtrl.postChangePassword);
router.get('/:code/verify', authCtrl.getVerifyUser);
router.get('/logout', authCtrl.getLogout);

module.exports = router;