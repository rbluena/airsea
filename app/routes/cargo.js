const express = require('express');
const cargoCtrl = require('./../controllers/cargo');
const passport = require('../config/passport');
const router = express.Router();

router.get('/', cargoCtrl.getIndex);
router.get('/create', cargoCtrl.getCreate);

module.exports = router;