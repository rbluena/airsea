const express = require('express');
const bondCtrl = require('./../controllers/bond');
const passport = require('../config/passport');
const router = express.Router();

router.get('/', passport.isAuthenticated, passport.isAdmin, bondCtrl.getIndex);
router.get('/create', passport.isAuthenticated, passport.isAdmin, bondCtrl.getCreate);
router.post('/create', passport.isAuthenticated, passport.isAdmin, bondCtrl.postCreate);
router.get('/usage', passport.isAuthenticated, passport.isAdmin, bondCtrl.getUsage);
router.get('/edit/:id', passport.isAuthenticated, passport.isAdmin, bondCtrl.getEdit);
router.post('/update', passport.isAuthenticated, passport.isAdmin, bondCtrl.postUpdate);
router.get('/:id/edit', passport.isAuthenticated, passport.isAdmin, bondCtrl.getEdit);
router.get('/:id/delete', passport.isAuthenticated, passport.isAdmin, bondCtrl.getDelete);

module.exports = router;