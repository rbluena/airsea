const express = require('express');
const passport = require('../config/passport');
const filesCtrl = require('./../controllers/files');
const router = express.Router();

router.post('/excel/import', passport.isAuthenticated, passport.isAdmin, filesCtrl.uploadExlBond);

module.exports = router;