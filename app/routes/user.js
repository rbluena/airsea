const express = require('express');
const userCtrl = require('./../controllers/user');
const passport = require('../config/passport');
const router = express.Router();

router.get('/', passport.isAuthenticated, userCtrl.getIndex);
router.get('/me', passport.isAuthenticated, userCtrl.getProfile);
router.get('/create', userCtrl.getCreate);
router.get('/changepassword', passport.isAuthenticated, userCtrl.getChangePassword);
router.get('/:id/edit', passport.isAuthenticated, userCtrl.getEdit);
// router.put('/:id/edit', userCtrl.putEdit);
router.get('/:id/admin', passport.isAuthenticated, userCtrl.getAssignAdmin);
router.get('/assign/:username/admin', passport.isAuthenticated, userCtrl.getAssignAdminByUsername);
router.get('/:id/remove_admin', passport.isAuthenticated, passport.isAdmin, userCtrl.getRemoveAdmin);
router.get('/:id/remove', passport.isAuthenticated, passport.isAdmin, userCtrl.getRemove);
router.get('/clients', passport.isAuthenticated, passport.isAdmin, userCtrl.getClients);
router.get('/clients/add', passport.isAuthenticated, passport.isAdmin, userCtrl.getCreateClient);

module.exports = router;