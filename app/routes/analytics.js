const express = require('express');
const passport = require('../config/passport');
const analyticsCtrl = require('./../controllers/analytics');
const router = express.Router();

router.get('/bond_usage', passport.isAuthenticated, passport.isAdmin, analyticsCtrl.getBondUsage);

module.exports = router;