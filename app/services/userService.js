const Q = require('q');
const User = require('../models/User');

module.exports = function() {
	return {

		getStaff: async function() {
			const deferred = Q.defer();

			try {
				const users = await User.find({ 'organization.internal': true });

				if(!users) {
					console.log('++++++ Failed to find users +++++++');
					console.log(users);
					deferred.reject('Failed to find users!');
				}

				deferred.resolve(users);
			} catch (err) {
				console.log('++++++++ FAILED TO FIND USER +++++++++++');
				deferred.reject(err);
			}

			return deferred.promise;
		},
		
		getClients: async function() {
			const deferred = Q.defer();

			try {
				const clients = await User.find({ 'organization.internal': false, deleted: false });

				if(!clients) {
					console.log('++++++ Failed to find clients +++++++');
					console.log(clients);
					deferred.reject('Failed to find clients!');
				}

				deferred.resolve(clients);
			} catch (err) {
				console.log('++++++++ FAILED TO FIND USER +++++++++++');
				deferred.reject(err);
			}

			return deferred.promise;
		},

		create: async function(user) {
			const deferred = Q.defer()
			try {
				const user = await User.create(user);
				deferred.resolve(user);
			} catch (err) {
				console.log('+++ error occured when creating user ++++++');
				deferred.reject(err);
			}

			return deferred.promise;
		},

		assignAdmin: async function(userId) {
			const deferred = Q.defer();
			try {
				const assignedUser = await User.findOneAndUpdate({_id: userId}, {level: ['staff', 'admin']});
				
				if(!assignedUser) {
					console.log('++++++ Something went wrong updating user +++++++++++');
					console.log(assignedUser)
					deferred.reject('Failed to assign admin');
				}

				deferred.reject('Failed to assing admin!')
				console.log('+++++++ user assigned admin prevalage ++++++++');
				deferred.resolve(assignedUser);
			} catch (err){
				console.log('++++++++++ err occured when assigning user admin +++++++')
				console.log(err);
				deferred.reject('Failed to assign admin');
			}

			return deferred;
		},
		
		assignAdminByUsername: async function(username) {
			const deferred = Q.defer();
			try {
				const assignedUser = await User.findOneAndUpdate({ username: username}, {level: ['staff', 'admin']});
				
				if(!assignedUser) {
					console.log('++++++ Something went wrong updating user +++++++++++');
					console.log(assignedUser)
					deferred.reject('Failed to assign admin');
				}

				deferred.reject('Failed to assing admin!')
				console.log('+++++++ user assigned admin prevalage ++++++++');
				deferred.resolve(assignedUser);
			} catch (err){
				console.log('++++++++++ err occured when assigning user admin +++++++')
				console.log(err);
				deferred.reject('Failed to assign admin');
			}

			return deferred;
		},

		removeAdmin: async function(userId) {
			const deferred =  Q.defer();

			try {
				const admins = await User.find({
					level: 'admin'
				});

				if (admins.length < 2) {
					console.log('++++++++ At least one admin should remain +++++++++');
					deferred.reject('At least should be one admin, assign another admin before removal!');
				} else {
					const removedAdmin = await User.findOneAndUpdate({ _id: userId }, { level: ['user'] });
					
					if(!removedAdmin) {
						console.log('++++++++ Failed to an unasign user admin +++++++');
						console.log(removedAdmin)
						deferred.reject('Failed to unassign admin');
					}

					deferred.resolve(removedAdmin);
				}
			} catch (err) {
				console.log(err);
				deferred.reject('Failed to unassign user from admin');
			}
			
			return deferred.promise;
		},

		deleteUser: async function(userId) {
			try {
				const deletedUser = await User.findOneAndRemove({ _id: userId });

				if(!deletedUser) {
					console.log(deletedUser);
				}

				return deletedUser;
			} catch (err) {
				console.log('+++++++++ Failed to remove user +++++++++');
				console.log(err)
				return err
			}
		}
	}
}