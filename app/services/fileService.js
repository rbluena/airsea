const Q = require('q');
const _ = require('lodash');
const Transit = require('../models/Transit');

module.exports = function() {
	return {
		importExcel: async function(data) {
			const deferred = Q.defer();

			try {
				let importedData = [];
				_.each(data, async function(newData) {
					const foundData = await Transit.findOne({ subTeeOne: newData.subTeeOne });
					if(foundData) {
						const updated = await Transit.update({ subTeeOne: newData.subTeeOne }, { $set: {issueDate: newData.issueDate }});
						importedData.push(updated);
					} else {
						const inserted = await Transit.create(newData);
						importedData.push(inserted)
					}
				});

				deferred.resolve(importedData);		
			} catch(err) {
				console.log('+++++++++ error occured +++++++');
				console.log(err);
				deferred.reject(err);
			}

			return deferred.promise;
		}
	}
}