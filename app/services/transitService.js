const Q = require('q');
const Transit = require('../models/Transit');

module.exports = function(User) {
	return {
		importExcel: async function() {

		},

		create: async function(user) {
			const deferred = Q.defer()
			try {
				const user = await User.create(user);
				defer.resolve(user);
			} catch (err) {
				console.log('+++ error occured when creating user ++++++');
				defer.reject(err);
			}
		},

		assignAdmin: async function(userId) {
			try {
				const user = await User.findOne({ _id: userId });
				user.level.push('admin');
				const updatedUser = user.save();

				if(!updatedUser) {
					console.log('++++++ Something went wrong updating user +++++++++++');
				}
				console.log('+++++++ user assigned admin prevalage ++++++++');
				return updatedUser;
				
			} catch (err){
				console.log('++++++++++ err occured when assigning user admin +++++++')
				consol.log(err);
			}
		}
	}
}