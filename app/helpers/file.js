module.exports = {
  getCountry: function(code) {
    const countries = [
      { code: 'BI', name: 'Burundi' }, 
      { code: 'IN', name: 'India' }, 
      { code: 'CH', name: 'China' }, 
      { code: 'RW', name: 'Rwanda' }, 
      { code: 'TZ', name: 'Tanzania' },
      { code: 'UG', name: 'Uganda' },
      { code: 'US', name: 'United States' },
      { code: 'ZM', name: 'Zambia' },
      { code: 'CD', name: 'Canada' }
    ];

    if(!code || typeof code !== 'string') {
      return {
        code: 'TZ',
        name: 'Tanzania'
      }
    }
    
    const country = countries.filter(function(country) {
      return country.code === code;
    });

    return country[0];
  }
}