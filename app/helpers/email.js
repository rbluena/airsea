const sgMail = require('@sendgrid/mail');
sgMail.setApiKey(process.env.SENDGRID_API_KEY);


module.exports = {
  sendEmail: async function(options) {
    try {
      const { to, from, subject, text, html } = options;
      const msg = { to, from, subject, text, html };
      
      return sgMail.send(msg);
    } catch (err) {
      console.log(err);
      return err;
    }
  }
}