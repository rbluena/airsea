const moment = require('moment');
const Bond = require('../models/Bond');
const Transit = require('../models/Transit');


module.exports = {
  getBondUsage: async function() {
    const transitsUsingBonds = await Transit.find({'bondValidation.status': false, delete: false}).lean();
    const bonds =  await Bond.find({ active: true }).lean();

    const totalBonds = bonds.reduce(function(sum, next) {
      sum = sum + next.amount;
      return sum
    }, 0);

    const usedBonds = transitsUsingBonds.reduce(function(sum, next) {
      sum = sum + next.bondValue;
      return sum;
    }, 0);

    return {
      totalBonds,
      usedBonds
    }
  },

  getBondUsageByCountry: async function(from, to) {
    try {
      const query = {}; // Based on issueDate
      query.bondInUse = true;

      const transitsForBond = await Transit.find(query, {destinationCountry: 1, bondValue: 1}).lean();
    } catch (err) {
      console.log(err);
    }
  },

  getBondUsageByMonth: async function(from, to) {
    try {
      const transitsForBond = await Transit.find({'bondValidation.status': false}).lean();
      
      const bonds = await Bond.find({activeStatus: true}).lean();
      const totalBonds = bonds.reduce(function(sum, next) {
        sum = sum + next.amount;
        return sum
      }, 0);
  
      const usedBonds = transitsForBond.reduce(function(sum, next) {
        sum = sum + next.bondValue;
        return sum;
      }, 0);
  
      let bondBalance = totalBonds - usedBonds;      
    } catch (err) {
      console.log(err);
      return err;
    }
  },
  
  getBondUsageByCountryMonthly: function(month) {
    return 'Something';
  },
  
  getBondUsageByCountryYearly: function(year) {
    return 'Something';
  },
}