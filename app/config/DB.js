const mongoose = require('mongoose');
const Promise = require('bluebird');

module.exports = () => {
  const db = mongoose;
  db.Promise = global.Promise;
  db.connect(process.env.MONGODB_URI, { useMongoClient: true });
  db.connection
    .once('open', () => {
      console.log('MongoDB connected successfully!')
    })
    .on('error', (err) => {
      console.log('MongoDB failed to connect!', err);
    });
};
