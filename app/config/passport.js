const passport = require('passport');
const passportLocal = require('passport-local');
const User = require('../models/User');

const LocalStrategy = passportLocal.Strategy;

passport.serializeUser((user, done) => {
  done(null, user.id);
});

passport.deserializeUser((id, done) => {
  User.findById(id, (err, user) => {
    done(err, user);
  });
});

const strategyCallback = async (req, username, password, done) => {
  try {
    const user = await User.findOne({
      $or: [{ username }, { email: username }, { phone: username }]
    });

    console.log(user);

    if (!user) return done(null, false, {msg: `User with that credentails not found. You can use your username, email or phone as username to login.`});

    const isMatch = await User.comparePassword(password, user.password);

    if (!isMatch) return done(null, false, { msg: 'Invalid phone or password.' });
    return done(null, user, { msg: 'Welcome back!' });
    
  } catch (err) {
    return done(err);
  }
}

const localStrategy = new LocalStrategy({
  usernameField: 'username',
  passReqToCallback: true
}, strategyCallback);

passport.use(localStrategy);

exports.isAuthenticated = (req, res, next) => {
  if (req.isAuthenticated()) {
    return next();
  }
  res.redirect('/');
};

exports.isAdmin = (req, res, next) => {
  if(req.isAuthenticated()) {
    const isAdmin = req.user.level.includes('admin')
    if(!isAdmin) {
      req.flash('warning', {
        msg: 'You are not authorize to perform this action.'
      })
      return res.redirect('back');
    }
    console.log(isAdmin);
    return next();
    }
  return res.status(300).redirect('/');
}

exports.isAdmin = (req, res, next) => {
  if(req.isAuthenticated()) {
    const isStaff = req.user.organization.internal;

    if(!isStaff) {
      req.flash('warning', {
        msg: 'You have no authorization, please contact administrator.'
      })
      return res.redirect('back');
    }
    return next();
  }
  req.flash('errors', {
    msg: 'You need to login to view this page.'
  });
  
  return res.status(300).redirect('/');
}