const moment = require('moment');
const accounting = require('accounting');
const path = require('path');
const pug = require('pug');
const Bond = require('./../models/Bond');
const { getBondUsage } = require('../helpers/analytics'); 

exports.getIndex = async (req, res, next) => {
  try {
    const bonds = await Bond.find().sort({validFrom: '-1'});
    const { totalBonds, usedBonds } = await getBondUsage();
    const bondBalance = totalBonds - usedBonds;

    res.render('bond/pages', {
      title: 'Bond',
      headerTitle: 'Bond',
      subHeader: 'Summary',
      sidebarActive: 'bond',
      activeTab: 'home',
      totalBonds,
      usedBonds,
      bondBalance,
      bonds
    });
  } catch (err) {
    console.log(err);
    res.render('errors/500');
  }
};


exports.getCreate = async (req, res, next) => {
  try {    
    res.render('bond/pages', {
      title: 'Bond',
      headerTitle: 'Bond',
      subHeader: 'Add',
      sidebarActive: 'bond',
      activeTab: 'create',
    });

  } catch (err) {
    
  }
};


exports.getEdit = async (req, res, next) => {
  try {
    const bond = await Bond.findById(req.params.id);

    if(!bond) {

    }
    res.render('bond/pages', {
      title: 'Bond',
      headerTitle: 'Bond',
      subHeader: 'Edit',
      sidebarActive: 'bond',
      activeTab: 'edit',
      bond,
    });
  } catch (err) {
    
  }
};

exports.postCreate = async (req, res, next) => {
  try {
    const data = req.body
    data.amount = accounting.unformat(data.amount);

    // if(new Date(data.validTo).getTime() < new Date().getTime()) {
    //   data.active = false;
    // }

    console.log(data);

    const bond = await Bond.create(data);

    if (!bond) {
      req.flash('errors', {
        msg: 'Bond was not created, please try again!'
      });

      res.redirect('back');
    }

    req.flash('success', {
      msg: 'Bond was created successfully!'
    });
    res.redirect('back')

  } catch (err) {
    console.log(err)
    red.render('errors/500');
  }
};

exports.getUsage = async (req, res, next) => {
  try {
    
    res.render('bond/pages', {
      title: 'Bond',
      headerTitle: 'Bond',
      subHeader: 'Usage',
      sidebarActive: 'bond',
      activeTab: 'usage',
      // bonds
    });
  } catch (err) {
    console.log(err);
    res.render('errors/500', {title: 'Error'});
  }
}

exports.postUpdate = async (req, res, next) => {
  try {
    const body = req.body;
    const bondId = req.body.bondId

    console.log('++++ BOND ID +++', bondId);

    const data = {
      securityNo: body.securityNo,
      amount: accounting.unformat(body.amount),
      validFrom: body.validFrom,
      validTo: body.validTo,
      active: body.active
    }

    if(new Date(body.validTo).getTime() < new Date().getTime()) {
      data.active = false;
    }

    const updated = await Bond.findOneAndUpdate({ _id: bondId }, data);

    console.log(updated);

    if(!updated) {
      req.flash('errors', {
        msg: 'Failed to update bond, please try again later!'
      });
    }

    req.flash('success', {
      msg: 'Success: Bond was updated!'
    });

    return res.redirect('back');
  } catch(err) {
    console.log(err);
    req.flash('errors', {
      msg: 'Failed to update, try again!'
    });

    return res.redirect('back');
  }
};

exports.getDelete = async (req, res, next) => {
  try {
    const bondId = req.params.id;
    const bondDeleted = await Bond.findOneAndRemove({ _id: bondId });

    if(!bondDeleted) {
      req.flash('errors', {
        msg: 'Failed to delete, try again!'
      });
      return res.redirect('back');
    }

    req.flash('success', {
      msg: 'Bond deleted successfully!'
    });
    
    return res.redirect('back');
  } catch (err) {
    console.log('+++++++++ ERROR OCCURED WHEN DELETING BOND ++++++')
    console.log(err);
    req.flash('errors', {
      msg: 'Failed to delete, try again!'
    });
    return res.redirect('back');
  }
}