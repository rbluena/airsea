const Transit = require('./../models/Transit');
const Bond = require('./../models/Bond');

const {
  getBondUsage,
  getBondUsageByMonth, // BarChart
  bondUsageByCountry, // BarChart This month, this year
} = require('../helpers/analytics')

exports.getBondUsage = async (req, res, next) => {
  try {
    const { totalBonds, usedBonds } = await getBondUsage();
    const bondBalance = totalBonds - usedBonds;

    res.status(200).send({totalBonds, usedBonds, bondBalance});
  } catch (err) {
    console.log(err);
    res.status(400).json('Failed to read analytics data');
  }
}

exports.getBondUsageByCountry = async (req, res, next) => {
  try {
    const bondUsage = await bondUsageByCountry();
  } catch (err) {
    
  }
}