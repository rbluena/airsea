// import lodash from 'lodash';
const passport = require('passport');
const bcrypt = require('bcryptjs');
const nodemailer = require('nodemailer');
const randomstring = require('randomstring');
const User = require('../models/User');
const Email = require('./../helpers/email');
require('../config/passport');

/**
 * Post registering new user
 */
exports.validateRegister = async (req, res, next) => {
  console.log(req.body);

  try {
    req.checkBody('firstname', 'Firstname is required').notEmpty();
    req.checkBody('lastname', 'Lastname is required').notEmpty();
    req.checkBody('phone', 'Phone number is required').notEmpty();
    req.checkBody('phone', 'Phone number at least 12 number.').len(12);
    req.checkBody('email', 'Email is required').notEmpty();

    // if(req.body.email) {
    //   req.checkBody('email', 'Email address for organization is required').notEmpty();
    // }

    req.sanitizeBody('email').normalizeEmail({
      remove_dots: false,
      remove_extension: false,
      gmail_remove_subaddress: false
    });

    const errors = req.validationErrors();

    if (errors) {
      errors.forEach((error) => {
        req.flash('errors', {
          msg: error.msg
        });
      });
      console.log(errors)
      return res.redirect('back');
    }

    next() // Proceed if data filled properly
  } catch (err) {
    console.log(err);
    res.render('error/500', {
      title: 'Error'
    });
  }
};

exports.validateLogin = async (req, res, next) => {
  try {
    if (req.isAuthenticated()) {
      res.redirect('/');
      return;
    }

    req.checkBody('username', 'Username or phone number is required').notEmpty();
    req.checkBody('password', 'Password must be at least 5 characters long').notEmpty();

    const errors = req.validationErrors();

    if (errors) {
      errors.forEach((error) => {
        req.flash('errors', {
          msg: error.msg
        });
      });
      console.log(errors)
      return res.redirect('/');
    }

    next();
  } catch (err) {
    console.log(err);
    req.flash('errors', {
      msg: 'There was an error, please try again later.'
    });
    res.redirect('back');
  }
}

exports.postRegister = async (req, res, next) => {
  const {firstname, lastname, phone, email, level} = req.body;
  
  if(req.body.organization) {
    const organization = {
      internal: false,
      name: req.body.organization
    }
  }

  try {
    const userWithPhoneOrEmail = await User.findOne({
      $or: [{ phone }, {email}]
    });
    
    if (userWithPhoneOrEmail) {
      req.flash('errors', {
        msg: 'Account with that phone number or email already exists.'
      });
      return res.redirect('back');
    }
    
    const password = randomstring.generate(6);
    const verificationCode = randomstring.generate(16);
    const username = randomstring.generate(5);
    const salt = await bcrypt.genSalt(10);    
    const hash = await bcrypt.hash(password, salt);

    const newUser = new User();
    newUser.firstname = firstname;
    newUser.lastname = lastname;
    newUser.email = email;
    newUser.username = username;
    newUser.phone = phone;
    if(req.body.organization) {
      newUser.organization.internal = false
      newUser.organization.name = req.body.organization
    }
    newUser.verificationCode = verificationCode;
    newUser.password = hash;

    const createdUser = await newUser.save();

    if(!createdUser) {
      console.log(createdUser);

      req.flash('errors', {
        msg: 'User was not created. There was an error please try again later.'
      });
      return res.redirect('back');
    }

    const loginLink = `${process.env.SERVER_URL}`;
    const emailText = `Hello ${createdUser,firstname} \n\n
                        You have been invited by AirSea to join AirSea's online platform. The following are first time login credentails. \n\n
                        \s\s USERNAME: ${createdUser.email} \n
                        \s\s PASSWORD: ${password} \n\n
                        To login you need to click the link below.\n
                        ${loginLink}
                        AirSea company, \n
                        IT Department,\n
                        +255-744-233119
                        `;
    const emailHtml = `
                        <body>
                          <h4>Hello ${createdUser.firstname}</h4>\n\n
                          <div>
                            <p>You have been invited by AirSea to join AirSea's online platform. The following are just first-time login credentails. You
                            have to update your credentails after loggin in.</p>
                            <p><strong>USERNAME: ${createdUser.email}</strong></p>
                            <p><strong>PASSWORD: ${password}</strong></p>
                            <p>To login you need to click the link below.</p>
                            <p></p>
                            <h4><strong><a href=${loginLink}>CLICK HERE</a></strong></h4>

                            <p>Thank you!</p>
                            <br />
                            <p>AirSea Company,</p>
                            <p>IT Department,</p>
                            <p>+255-744-233119</p>
                          </div>
                        </body>
                        `;

    const emailContents = {
      to: createdUser.email,
      from: {
        email: 'no-reply@airseatz.com',
        name: "AirSea"
      },
      subject: 'You have been invited by AirSea Company',
      text: emailText,
      html: emailHtml
    }

    const emailSent = Email.sendEmail(emailContents);

    req.flash('success', {
      msg: 'User was created successfully, confirmation email is on the way!'
    });

    return res.render('users/pages', {
      title: 'AirSea - Add Staff',
      headerTitle: 'Staff',
      subHeader: 'Add',
      sidebarActive: 'users',
      activeTab: 'createUser',
      username: newUser.username,
      phone: newUser.phone,
      password: password
    });

  } catch (err) {
    console.log(err);
    req.flash('errors', {
      msg: 'User was not created. There was an error please try again later.'
    });
    res.redirect('back');
  }
}

exports.getRegister = (req, res, next) => {
  res.redirect('/users/create');
}

exports.postLogin = async (req, res, next) => {

  try {
    const {username, password} = req.body;
    passport.authenticate('local', (err, user, info) => {
      if (err) {
        req.flash('errors', {
          msg: 'Error occured, please try again!'
        });
        return res.status(300).redirect('back');
      }

      if (!user) {
        req.flash('errors', info);
        res.status(400).redirect('back');
        return;
      }

      req.logIn(user, (err) => {
        if (err) {
          req.flash('errors', {
            msg: 'Failed to login, please try again.'
          })
          req.status(500).redirect('back');
          return;
        }

        if (user.firstLogin) {
          req.flash('success', {
            msg: 'You are loggedin for the first time, Please update existing username and password.'
          });
          res.status(200).redirect('/users/changepassword');
          return;
        }

        res.redirect(req.session.returnTo || '/dashboard');
      })
    })(req, res, next);

  } catch (err) {
    console.log(err);    
  }
}

exports.validatePostChangePassword = (req, res, next) => {
  req.checkBody('password', 'Password is requred').notEmpty();
  req.checkBody('password', 'Password must be at least 5 characters long').len(5);

  // Matching password should be used later
  const errors = req.validationErrors();

  if (errors) {
    errors.forEach((error) => {
      req.flash('errors', {
        msg: error.msg
      });
    });
    console.log(errors)
    return res.redirect('back');
  }

  next();
};


exports.postChangePassword = async (req, res, next) => {
  try {
    const { password } = req.body;
    const username = req.body.username.trim().toLowerCase();

    console.log('+++++++++ USERNAME +++++++++', username);

    if(username.length < 5) {
      req.flash('errors', {
        msg: 'Username should be more than 5 characters.'
      });
      return res.redirect('back');
    }

    const foundUser =  await User.findOne({username: username });

    if(foundUser) {
      req.flash('errors', {
        msg: 'That username is not available. Choose another username!'
      });
      return res.redirect('back');
    }

    const user = req.user;

    const salt = await bcrypt.genSalt(10);
    user.password = await bcrypt.hash(password, salt);
    user.firstLogin = false;
    user.username = username;
    const savedUser = await user.save();

    req.flash('success', {
      msg: 'Your username and password has been updated successfully!'
    });
    res.redirect('/dashboard');
  } catch (err) {
    console.log(err);
    req.flash('errors', {
      msg: 'Error occured, please try again!'
    })
    res.redirect('back');
  }
}

exports.getVerifyUser = async (req, res, next) => {
  try {
    const { code } = req.params;
    const user = await User.findOne({ verificationCode: code });

    if(!user) {
      req.flash('errors', {
        msg: 'User not found!'
      });
      res.redirect('back');
      return;
    }

    user.verified = true
    const savedUser = await user.save();

    if (!savedUser) {
      req.flash('errors', {
        msg: 'User verification has been failed!'
      });

      res.redirect('back');
      return;
    }

    req.flash('success', {
      msg: `${user.firstname} ${user.lastname} has been verified!`
    });
    res.redirect('back');    
  } catch (err) {
    console.log(err)
  }  
}

/**
 * Logging user out
 */
exports.getLogout = (req, res) => {
  req.logout();
  req.flash('success', {
    msg: 'You are logged out!'
  });
  res.redirect('/');
};