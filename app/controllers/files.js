const path = require('path');
const moment = require('moment');
const Transit = require('./../models/Transit');
const { getCountry } = require('./../helpers/file');
const fs = require('fs');
const util = require('util');
const XLSX = require('xlsx');
const formidable = require('formidable');
const form = new formidable.IncomingForm();
const uploadDir = path.join(__dirname, './../../uploads');
form.encoding = 'utf-8';
form.uploadDir = uploadDir;
form.keepExtensions = true;

exports.uploadExlBond = async (req, res, next) => {
  try {

    let workbook;

    form.on('file', function (field, file) {

      if(file.size === 0) {
        req.flash('errors', {
          msg: 'File was not selected!'
        });
        return res.redirect('back');
      }

      fs.rename(file.path, form.uploadDir + "/" + file.name, async function (err) {

        workbook = XLSX.readFile(`${uploadDir}/${file.name}`);
        const transitListSheet = workbook.Sheets[workbook.SheetNames[0]];
        var jsonData  = XLSX.utils.sheet_to_json(transitListSheet);
        let newData = {};

        var mappedData = jsonData.map((data) => {
          let issueDate;
          let departureDate;
          let borderPointDate;
          let checkPoint1Date;
          let checkPoint2Date;
          let bondValidation = {
            status: false,
            validatedAt: null
          };

          if(data['Issue Date'] !== undefined) {
            issDate = moment(data['Issue Date'], 'DD/MM/YYYY').format();
            issueDate = issDate !== 'Invalid date' ? issDate : null;
          } else {
            issueDate = null;
          }

          if(data['Departure Date'] !== undefined) {
            const departDate = moment(data['Departure Date'], 'DD/MM/YYYY').format();
            departureDate = departDate !== 'Invalid date' ? departDate : null;
          } else {
            departureDate = null;
          }
          
          if(data['Check Point-1 Date'] !== undefined) {
            const date = moment(data['Check Point-1 Date'], 'DD/MM/YYYY').format();
            checkPoint1Date = date !== 'Invalid date' ? date : null;
          } else {
            checkPoint1Date = null;
          }
          
          if(data['Check Point-2 Date'] !== undefined) {
            const date = moment(data['Check Point-1 Date'], 'DD/MM/YYYY').format();
            checkPoint2Date = date !== 'Invalid date' ? date : null;
          } else {
            checkPoint2Date = null;
          }
          
          
          if(data['Border Point/Via-Warehouse1 Date'] !== undefined) {
            const validationDate = moment(data['Border Point/Via-Warehouse1 Date'], 'DD/MM/YYYY').format();
            if(validationDate !== 'Invalid date')  {
              bondValidation = {status: true, validatedAt: validationDate};
            }
          }

          const clientEmail = data['email'] ? data['email'] : '';
          let destCountry = getCountry(data['Destination Country']);

          if(!destCountry) {
            destCountry = {
              code: data['Destination Country'],
              name: data['Destination Country']
            }
          }

          let bondVal = parseInt(data['Bond Value']);
          const BTDeclaration = data['BT Declaration No'].slice(0, -1);

          return {
            regime: data['Regime'],
            clientEmail: clientEmail,
            teeOne: data['T1'],
            subTeeOne: data['Sub-T1'],
            tansadNo: data['Tansad No'],
            BTDeclaration: BTDeclaration,
            CFANumber: "",
            CFAName: "",
            bondValue: bondVal || 0,
            goodsDesc: data['Goods Description'],
            CRN: data['CRN'],
            loadPackage: data['Load Package'],
            loadUnit: data['Package Unit'],
            loadWeight: data['Load Weight'],
            weightUnit: data['Weight Unit'],
            issueDate: issueDate,
            departureDate: departureDate,
            containerNo: data['Container No(Chassis No)'],
            vehicleNo: data['Vehicle No'],
            trailerNo: data['Trailer No'],
            semiTrailerNo: data['Semi-Trailer No'] || null,
            transporter: data['Transporter'],
            driverName: data['Driver Name'],
            driverLicense: data['Driver License'],
            portOfDeparture: data["Port of Departure/Via-Warehouse1"],
            checkPoint1: data["Check Point-1"],
            checkPoint1Date,
            checkPoint1Time: data["Check Point-1 Time"],
            checkPoint2: data['Check Point-2'],
            checkPoint2Date,
            checkPoint2Time: data["Check Point-2 Time"],
            borderPoint: data["Border Point/Via-Warehouse1"],
            borderPointDate: bondValidation.validatedAt,
            borderPointTime: data["Border Point/Via-Warehouse1 Time"],
            bondValidation,
            destinationCountry: destCountry
          }
        });

        const importedExcel = await global.fileService.importExcel(mappedData);
        
        if(!importedExcel.length) {
          req.flash('warning', {
            msg: 'No data has been imported!'
          });

          return res.redirect('back');
        }

        req.flash('success', {
          msg: 'Data uploaded successfully!'
        });
        return res.redirect('back');
      });
    });

    form.parse(req, function (err, fields, files) {
      if (err) {
      }
    });
  } catch(err) {
    console.log('+++++++++ EERROR WHILE UPLOADING EXCEL +++++++++++++++++');
    console.log(err);
    return res.redirect('back');
  }
};