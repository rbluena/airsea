const Transit = require('./../models/Transit');

exports.getIndex = async (req, res, next) => {
  
  try {
    const cargos = await Transit.find();
    res.render('cargo/pages', {
      title: 'Cargo Management',
      headerTitle: 'Cargo Management',
      subHeader: 'List',
      sidebarActive: 'cargo',
      activeTab: 'home',
      cargos
    });

  } catch (err) {
    
  }
};

/**
 * Addin new user in the system
 */
exports.getCreate = (req, res, next) => {
  res.render('cargo/pages', {
    title: 'Cargo',
    sidebarActive: 'cargo',
    activeTab: 'addCargo'
  });
};


/**
 * Edit new user in the system
 */
exports.getEdit = (req, res, next) => {
  res.render('users/edit', {
    title: 'AirSea - Edit'
  });
};