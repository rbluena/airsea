const moment = require('moment');
const fs = require('fs');
const util = require('util');
const path = require('path');
const zipcelx = require('zipcelx');
const XLSX = require('xlsx');
const _ = require('lodash');
const Transit = require('./../models/Transit');
const Bond = require('./../models/Bond');
const {
  getBondUsage,
  getBondUsageByMonth, 
  bondUsageByCountry, 
} = require('../helpers/analytics')

const downloadDir = path.join(__dirname, './../../downloads');

exports.getIndex = async (req, res, next) => {
  try {
    const data = req.query || {};
    const dbQuery = {};
    let issueDate = {};
    let departDate = {};

    if(data.bondValidation && data.bondValidation !== 'all') {
      dbQuery['bondValidation.status'] = data.bondValidation;
    }
    
    if(data.status && data.status !== 'all') {
      dbQuery['status'] = data.status;
    }

    if (data.regime) {
      if(data.regime !== 'all' || !data.regime.includes('all')) {
        dbQuery['regime'] = data.regime;
      }
    }

    if (data.destCountry) {
      dbQuery['destinationCountry.code'] = data.destCountry;
    }

    if (data.issueDateStart) {
      Object.assign(issueDate, { '$gte': moment(data.issueDateStart).format() });
    }

    if (data.issueDateEnd) {
      Object.assign(issueDate, { '$lte': moment(data.issueDateEnd).format() });
    }

    if (data.departDateStart) {
      Object.assign(departDate, { '$gte': moment(data.departDateStart).format() });
    }

    if (data.departDateEnd) {
      Object.assign(departDate, { '$lte': moment(data.departDateEnd).format() });
    }

    if (!_.isEmpty(departDate)) {
      dbQuery['departureDate'] = departDate;
    }

    if (!_.isEmpty(issueDate)) {
      dbQuery['issueDate'] = issueDate;
    }

    const transits = await Transit.find({ $or: [dbQuery], delete: false }).sort({ issueDate: -1 }).lean();

    res.render('clearance/pages', {
      title: 'Clearance',
      headerTitle: 'Clearance',
      subHeader: 'List',
      sidebarActive: 'clearance',
      activeTab: 'home',
      transits,
      query: data,
    });
  } catch (err) {
    console.log('+++++++ ERROR WHEN CALLING DATA ++++++++')
    console.log(err)
    res.redirect('back');
  }
};

exports.getShow = async (req, res, next) => {
  try {
    const transit = await Transit.findOne({ _id: req.params.id });

    if (!transit) {
      console.log('++++++++++++++= NO TRANSIT FOUND ++++++++++')
      console.log(transit);
      req.flash('errors', {
        msg: 'Transit was not found!'
      });
      return res.redirect('back');
    }

    res.render('clearance/pages', {
      title: 'Clearance',
      headerTitle: 'Clearance',
      subHeader: 'Show',
      sidebarActive: 'clearance',
      activeTab: 'showTransit',
      transit
    });
  } catch (err) {
    console.log('+++++++++ ERROR OCCURED SHOWING TRANSIT +++++')
    res.redirect('back');
  }
}

exports.getEdit = async (req, res, next) => {
  try {
    const transit = await Transit.findOne({ _id: req.params.id });

    if(!transit) {
      console.log('++++++++++++++= NO TRANSIT FOUND ++++++++++')
      console.log(transit);
      req.flash('errors', {
        msg: 'Transit was not found!'
      });
      return res.redirect('back');
    }

    res.render('clearance/pages', {
      title: 'Clearance',
      headerTitle: 'Clearance',
      subHeader: 'Edit transit',
      sidebarActive: 'clearance',
      activeTab: 'edit',
      transit,
      // availableBond,
      // totalUsedBond,
      // bondBalance,
    });
  } catch(err) {
    console.log('++++++++++ ')
  }
};

exports.postUpdate = async (req, res, next) => {
  try {
    const transit = req.body;
    const updated = await Transit.findOneAndUpdate({ _id: req.params.id }, transit);

    if(!updated) {
      console.log('+++++++ FAILED TO UPDATE +++++')
      console.log(updated);
      req.flash('errors', {
        msg: 'Failed to update please try again!'
      });
      res.redirect('back');
    }

    req.flash('success', {
      msg: 'Transit was updated successful!'
    });
    res.redirect('back');
  } catch(err) {
    console.log('+++++++++++++++++++ ')
    console.log(err);
    res.redirect('back');
  }
};

exports.getTransits = async (req, res, next) => {

  try {
    let query = {};

    const transits = await Transit.find(query).sort({ issueDate: -1 });

    return res.status(200).json(transits);
  } catch (err) {
    console.log('+++++++ ERROR WHEN CALLING DATA ++++++++')
    console.log(err)
    return res.status(500).json("Error: Occured...")
  }
};

/**
 * Delete transit
 */
exports.getDelete = async (req, res, next) => {
  try {
    // const deleted = await Transit.findOneAndRemove({ _id: req.params.id });
    const trashed = await Transit.update({ _id: req.params.id }, { $set: { delete: true }});

    console.log('++++++++ TRASHED DATA ++++++++', trashed);

    if(!trashed) {
      console.log('Failed to delete transite!');
      console.log(trashed);
      req.flash('errors', {
        msg: 'Failed to delete transit!'
      });

      return res.redirect('back');
    }

    req.flash('success', {
      msg: 'Transit deleted successfull!'
    });
    res.redirect('back');
  } catch (err) {
    req.flash('errors', {
      msg: 'Failed to delete transit!'
    });

    console.log('+++++++ ERR OCCORED WHEN DELETING TRANSIT ++++++++++');
    console.log(err);

    return res.redirect('back');
  }
};

exports.getImport = (req, res, next) => {
  res.render('clearance/pages', {
    title: 'Clearance',
    headerTitle: 'Clearance',
    subHeader: 'Import Excel',
    sidebarActive: 'clearance',
    activeTab: 'import'
  });
};

exports.getExport = async (req, res, next) => {
  try {
    const transitsForBond = await Transit.find().sort({ issueDate: -1 }).lean();
    
        const bonds = await Bond.find({activeStatus: true}).lean();
        const availableBond = bonds.reduce(function(sum, next) {
          sum = sum + next.amount;
          return sum
        }, 0);
        const totalUsedBond = transitsForBond.reduce(function(sum, next) {
          sum = sum + next.bondValue;
          return sum;
        }, 0);
        let bondBalance = parseInt(availableBond) - parseInt(totalUsedBond);
    
        const data = req.query || {};
        let dbQuery = {};
        let issueDate = {};
        let departDate = {};

        if(data.bondValidation && data.bondValidation !== 'all') {
          dbQuery['bondValidation.status'] = data.bondValidation;
        }
        
        if(data.status && data.status !== 'all') {
          dbQuery['status'] = data.status;
        }
    
        if (data.regime) {
          if(data.regime !== 'all' || !data.regime.includes('all')) {
            dbQuery['regime'] = data.regime;
          }
        }
    
        if (data.destCountry) {
          if (data.destCountry) {
            dbQuery['destinationCountry.code'] = data.destCountry;
          }
        }
    
        if (data.issueDateStart) {
          Object.assign(issueDate, { '$gte': moment(data.issueDateStart).format() });
        }
    
        if (data.issueDateEnd) {
          Object.assign(issueDate, { '$lte': moment(data.issueDateEnd).format() });
        }
    
        if (data.departDateStart) {
          Object.assign(departDate, { '$gte': moment(data.departDateStart).format() });
        }
    
        if (data.departDateEnd) {
          Object.assign(departDate, { '$lte': moment(data.departDateEnd).format() });
        }
    
        if (!_.isEmpty(departDate)) {
          dbQuery['departureDate'] = departDate;
        }
    
        if (!_.isEmpty(issueDate)) {
          dbQuery['issueDate'] = issueDate;
        }
    
        const transits = await Transit.find({ $or: [dbQuery], delete: false }).sort({ issueDate: -1 }).lean();
        
        const mappedTransits = transits.map(function(data) {
          data.issueDate = data.issueDate ? moment(data.issueDate).format('DD/MM/YYYY')
                                          : 'Not Issued';
          data.bondValidationDate = data.bondValidation.validatedAt ? moment(data.bondValidation.validatedAt).format('DD/MM/YYYY')
                                          : '';
            
          data.departureDate = data.departureDate ? moment(data.departureDate).format('DD/MM/YYYY')
                                                  : 'Not Departed';
              
          data.exitStatus = data.exitStatus === true ? 'Exited' : 'No-Exited'
          data.bondValidit = data.bondValidation.status === true ? 'Validated' : 'Not Validated';

          return data;
        });
    
    res.render('clearance/pages', {
      title: 'Clearance',
      headerTitle: 'Clearance',
      subHeader: 'Export to excel',
      sidebarActive: 'clearance',
      activeTab: 'export',
      transits: mappedTransits,
      query: data
    });
  } catch (err) {
    console.log(err);
    res.redirect('back');
  }
};

exports.putStatus = async (req, res, next) => {
  try {
    const transitId = req.params.id;
    const status = req.body.statusValue;
    const updated = await Transit.findOneAndUpdate({ _id: transitId}, { $set: { status: status }});

    // console.log('+++++++ UPDATED ++++++', updated);
    if(!updated) {
      return res.status(500).json('Failed to update!');
    }

    res.status(200).json('Status was changed successfull!');
  } catch (err) {
    console.log(err);
    return res.status(500).json('Failed to update!');
  }
}

exports.putValidateBond = async (req, res, next) => {
  try {
    const transitId = req.body.transitId;
    const validatedAt = req.body.validatedAt;
    const bondValidation = {
      status: true,
      validatedAt: moment(validatedAt).format()
    }

    const updated = await Transit.findOneAndUpdate({ _id: transitId}, { $set: { bondValidation }});

    if(!updated) {
      return res.status(500).json('Failed to update transit.')
    }

    res.status(200).json('success');
  } catch (err) {
    console.log(err);
    res.status(500).json('Failed to update')
  }
}