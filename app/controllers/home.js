const Transit = require('./../models/Transit');
const Bond = require('./../models/Bond');
const passport = require('../config/passport');
const {
  getBondUsage,
  getBondUsageByMonth, // BarChart
  bondUsageByCountry, // BarChart This month, this year
  // mizigoByCountry //Map chart
} = require('../helpers/analytics')

exports.getIndex = (req, res, next) => {
  if (req.isAuthenticated()) {
    return res.redirect('/dashboard');
  }
  res.render('users/login', {
    title: 'AirSea - Login',
  });  
};

exports.getDashboard = async (req, res, next) => {
  try {
    const { totalBonds, usedBonds } = await getBondUsage();
    const bondBalance = totalBonds - usedBonds;

    const subHeader = req.user.level.includes('admin') ? 'Analytics' : req.user.firstname;

    res.render('home/pages', {
      title: 'Dashboard',
      headerTitle: 'Dashboard',
      subHeader,
      sidebarActive: 'home',
      activeTab: 'bond',
      totalBonds,
      usedBonds,
      bondBalance
    });
  } catch (err) {
    console.log(err);
    // res.redirect('back');
  }
}