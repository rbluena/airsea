const User = require('../models/User');

/**
 * Show all users
 */
exports.getIndex = async (req, res, next) => {

  try {
    const users = await global.userService.getStaff();
    
    res.render('users/pages', {
      title: 'AirSea - Staff',
      headerTitle: 'Staff',
      subHeader: 'All',
      sidebarActive: 'users',
      activeTab: 'allUsers',
      users,
    });
  } catch (err) {
    console.log(err);
    const errMsg = !Object.is(err) ? err : 'Failed pull users!';

    req.flash('errors', {
      msg: errMsg
    });
    res.render('errors/500', {
      title: 'Error: Error occured!'
    })
  }
};

exports.getProfile = (req, res, next) => {
  try {
    const user =  req.user;

    res.render('users/pages', {
      title: `AirSea - ${user.firstname} ${user.lastname}`,
      headerTitle: user.firstname,
      subHeader: '',
      sidebarActive: 'profile',
      activeTab: 'profile',
    });
  } catch (err) {
    console.log(err);
    res.render('errors/500', {
      title: 'Error occured'
    })
  }
}

/**
 * Adding new user in the system
 */
exports.getCreate = (req, res, next) => {
  res.render('users/pages', {
    title: 'AirSea - Add Staff',
    headerTitle: 'Staff',
    subHeader: 'Add',
    sidebarActive: 'users',
    activeTab: 'createUser'
  });
};

/**
 * Adding new client
 */
exports.getCreateClient = (req, res, next) => {
  res.render('users/pages', {
    title: 'AirSea - Add Client',
    headerTitle: 'Users',
    subHeader: 'Add client',
    sidebarActive: 'users',
    activeTab: 'createClient'
  });
};

exports.getClients = async function(req, res, next) {
  try {
    const users = await global.userService.getClients();
    
    res.render('users/pages', {
      title: 'AirSea - Clients',
      headerTitle: 'Users',
      subHeader: 'Add client',
      sidebarActive: 'users',
      activeTab: 'clients',
      users
    });
  } catch (err) {
    
  }
}

exports.getAssignAdmin = async (req, res, next) => {
  try {
    const userId = req.params.id;
    const assignedUser = global.userService.assignAdmin(userId);

    if(!assignedUser) {
      return req.flash('errors', {
        msg: 'Failed to assign user to admin, try again later!'
      });
      
      return res.redirect('back');
    }

    req.flash('success', {
      msg: 'User assigned to admin successfull!'
    });

    return res.status(200).redirect('back');
  } catch (err) {
    console.log('++++++ Error occured +++++++');
    console.log(err);
  }
};

exports.getAssignAdminByUsername = async (req, res, next) => {
  try {
    const username = req.params.username;
    const assignedUser = await global.userService.assignAdminByUsername(username);

    if(!assignedUser) {
      return req.flash('errors', {
        msg: 'Failed to assign user to admin, try again later!'
      });
      
      return res.redirect('back');
    }

    req.flash('success', {
      msg: 'User assigned to admin successfull!'
    });

    return res.redirect('back');

  } catch (err) {
    console.log('++++++ Error occured +++++++');
    console.log(err);
    res.redirect('back');
  }
}

exports.getRemoveAdmin = async (req, res, next) => {
  try {
    const userId = req.params.id;
    const removedAdmin = await global.userService.removeAdmin(userId);

    console.log('++++++ REMOVED ADMIN ++++++++', removedAdmin);

    if (!removedAdmin) {
      req.flash('errors', {
        msg: 'Failed to unassign this user from admin!'
      })
      return res.status(400).redirect('back')
    }

    req.flash('success', {
      msg: 'User unassigned from admin!'
    });
    
    return res.status(200).redirect('back');
  } catch (err) {
    console.log(err);

    const errMsg = !Object.is(err) ? err : 'Failed to remove admin, please try again later!';
    req.flash('errors', {
      msg: errMsg
    });
    res.redirect('back');
  }
};


exports.getRemove = async (req, res, next) => {
  try {
    const userId = req.params.id;
    const removedUser = await global.userService.deleteUser(userId);
    console.log('++++++ REMOVED USER ++++++++');

    if (!removedUser) {
      req.flash('errors', {
        msg: 'Failed to remove user completely!'
      })
      return res.status(400).redirect('back')
    }

    req.flash('success', {
      msg: 'User removed successfull!'
    });

    return res.status(200).redirect('back');
  } catch (err) {
    console.log(err);
    const errMsg = !Object.is(err) ? err : 'Failed to remove user completely!';

    req.flash('errors', {
      msg: errMsg
    });
    res.redirect('back');
  }
};


/**
 *User changing password when login for the first time
 */
exports.getChangePassword = (req, res, next) => {
  res.render('users/change_pass', {
    title: 'AirSea - Change Password',
    sidebarActive: 'users',
    activeTab: 'changePass'
  });
};


/**
 * Edit user in the system
 */
exports.getEdit = (req, res, next) => {
  res.render('users/edit', {
    title: 'AirSea - Edit'
  });
};