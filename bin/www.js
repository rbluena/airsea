const app = require('../app.js');
const PORT = process.env.PORT || 3300

app.listen(PORT, () => console.log(`Server is running on port ${process.env.PORT}`));
